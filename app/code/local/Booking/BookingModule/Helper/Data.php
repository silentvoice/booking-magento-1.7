<?php
/**
 * Created by PhpStorm.
 * User: projectx
 * Date: 28/5/14
 * Time: 11:09 PM
 */

class Booking_BookingModule_Helper_Data extends Mage_Core_Helper_Abstract {




    function validate_url($url)
    {

        if ( $url != 'http://localhost/test_availability.html')
        {
         //    return 3; //Invalid URL
               return 0;
        }
        else
        {
            return 0; //Success
        }
    }
    function validate_function_id($fnc_id)
    {
        if ( $fnc_id != '1' && $fnc_id != '2' && $fnc_id != '3' && $fnc_id != '4' && $fnc_id != '5' && $fnc_id != '6' && $fnc_id != '7')
        {
            return 4; //Invalid Function Id
        }
        else
        {
            return 0; //Success
        }
    }
    function validate_booking_id($booking_id)
    {
        return 0; //Success
    }
    function validate_ground_id($ground_id)
    {
        return 0; //Success
    }
    function validate_from_date($from_date)
    {
        $check = $this->validate_date($from_date);
        if( $check != '1')
        {
            return 5; // Invalid date value
        }
        else
        {
            $today = date("Y-m-d");
            $today_num = strtotime(''.$today.' 00:00:00');
            $from_date_num = strtotime(''.$from_date.' 00:00:00');
            if ( $from_date_num <= $today_num )
            {
                return 6; //Booking can start from tomorrow only
            }
            else
            {
                return 0; //Success
            }
        }
    }
    function validate_from_time($from_time)
    {
        $check = $this->validate_time($from_time);
        if( $check != '0')
        {
            return 7; //Invalid time value
        }
        else
        {
            return 0; //Success
        }
    }
    function validate_to_date($from_date,$to_date)
    {
        $check = $this->validate_date($to_date);
        if( $check != '1')
        {
            return 5; // Invalid date value
        }
        else
        {
            $from_date_num = strtotime(''.$from_date.' 00:00:00');
            $to_date_num = strtotime(''.$to_date.' 00:00:00');
            if ( $from_date_num >= $to_date_num )
            {
                return 8; //To date has to be greater that from date
            }
            else
            {
                return 0; //Success
            }
        }
    }
    function validate_to_time($from_time, $to_time)
    {
        $check = $this->validate_time($to_time);
        if( $check != '0')
        {
            return 7; //Invalid time value
        }
        else
        {
            $from_time_num = strtotime('1970-01-01 '.$from_time.'');
            $to_time_num = strtotime('1970-01-01 '.$to_time.'');
            if ( $from_time_num >= $to_time_num )
            {
                return 9; //To time has to be greater that from time
            }
            else
            {
                return 0; //Success
            }
        }
    }
    function validate_date($date)
    {
        $date_val = strtotime(''.$date.' 00:00:00');
        $year  = date("Y",$date_val);
        $month = date("m",$date_val);
        $day   = date("d",$date_val);
        $retur = checkdate($month ,$day ,$year );
        return $retur;
    }
    function validate_time($time)
    {
        $time_num = strtotime('1970-01-01 '.$time.'');
        $hour  = date("G",$time_num);
        $minute = date("i",$time_num);
        $second = date("s",$time_num);
        $error_hour = is_numeric($hour);
        $error_minute = is_numeric($minute);
        $error_second = is_numeric($second);
        if( $error_hour == '1' && $error_minute == '1' && $error_second == '1' )
        {
            if ( intval($hour) >= 0 && intval($hour) <= 24 && intval($minute) >= 0 && intval($minute) <= 59 && intval($second) >= 0 && intval($second) <= 59 )
            {
                return '0';
            }
            else
            {
                return '1';
            }
        }
        else
        {
            return "1";
        }
    }

    //Connect database

    function booking_db($fnc_id,$sub_id,$booking_id,$ground_id,$from_date,$from_time,$to_date,$to_time,$hours)
    {

        $con_min_time_slot = "06:30:01"; //1 hour time slot
        $con_min_confirmation_time = "05:32:00"; //2 minutes time limit
        //Get connection
        $bookings = Mage::getModel('bookingmodule/bookings');
        $temp_book = Mage::getModel('bookingmodule/temp');
        $con = $this->connect_booking_db();
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $write = $resource->getConnection('core_write');
        switch ($fnc_id)
        {
            case 1:
                //availability($from_date,$from_time,$to_time,$ground_id)
                //Convert date and time
                $available = $this->check_available($sub_id,$ground_id,$from_date,$from_time,$to_date,$to_time,$hours,$con);
                echo $available;
                break;

            case 2:
                //availability($from_date,$ground_id)
                //Get bookings for the respective ground
                $query = "SELECT * FROM bookings WHERE ground_id = ".$ground_id." AND ( s_date = '".$from_date."' OR e_date = '".$from_date."' )";
                $result = $read->fetchAll($query);


                $unavail = 0;

                //Get availability if exist
                if($row = mysqli_fetch_array($result))
                {
                    $unavail = 1;
                    echo 0;
                }

                if( $unavail == 0)
                {
                    //Delete expired bookings
                    $this->delete_expired_bookings($con);

                    $query = "SELECT * FROM temp_book WHERE ground_id = ".$ground_id." AND ( s_date = '".$from_date."' OR e_date = '".$from_date."' )";
                    $result = $read->fetchAll($query);

                    //Get availability if exist
                    if($row = mysqli_fetch_array($result))
                    {
                        $unavail = 1;
                        echo 0;
                    }
                }

                if( $unavail ==0 )
                {
                    echo 1;
                }
                break;

            case 3:
                //book_ground($booking_id,$ground_id,$from__date,$from__time,$hours)
                //Prepare end date and time
                if ( $sub_id == '1')
                {
                    $end_date_time = strtotime(''.$from_date.' '.$from_time.' + '.$hours.' hours');
                    $to_date = date("Y-m-d",$end_date_time);
                    $to_time = date("G:i:s",$end_date_time);
                }
                else if ( $sub_id == '2')
                {
                    $from_time = "00:00:00";
                    $to_date = $from_date;
                    $to_time = "23:59:59";
                }
                else if ( $sub_id == '3')
                {
                    $to_date = $from_date;
                }

                $available = $this->check_available('3',$ground_id,$from_date,$from_time,$to_date,$to_time,$con);
                if( $available != '1')
                {
                    echo '20';
                    exit;
                }
                //Save booking
                $query = "INSERT INTO bookings(booking_id,ground_id,s_date,s_time,e_date,e_time) VALUES (".$booking_id.",".$ground_id.",'".$from_date."','".$from_time."','".$to_date."','".$to_time."')";

                $result = $write->query($query);


                if($result)
                {
                    echo 1;
                }
                else
                {
                    echo 0;
                }
                break;

            case 4:
                //cancel_booking($booking_id)
                //Delete booking
                $query = "DELETE FROM bookings WHERE booking_id = ".$booking_id."";

                $result = $write->query($query);

                if($result)
                {
                    echo 1;
                }
                else
                {
                    echo 0;
                }

                break;

            case 5:
                //get_free_slots($from_date,$to_date,$ground_id)
                //Declare slots array
                $slots[] = array();

                //Set time
                $from_time = "00:00:00";
                $to_time = "23:59:59";

                //Set minimum time difference
                $min_time_diff = strtotime('1970-01-01 '.$con_min_time_slot.'');

                //Convert date and time
                $booking_start = strtotime(''.$from_date.' '.$from_time.'');
                $booking_end = strtotime(''.$to_date.' '.$to_time.'');

                //Get bookings for the respective ground
                $query = "SELECT * FROM bookings WHERE ground_id = ".$ground_id." ORDER BY s_date, s_time";
                $result =$read->query($query);

                //Set start variable
                $slot_start = $booking_start;
                $slot_end = '';

                //Flag variables
                $count = 0;
                $flag  = 0;

                //Get free slots if exist
                foreach($result as $row)
                {
                    $flag = 1;
                    $start = strtotime(''.$row['s_date'].' '.$row['s_time'].'');
                    $end = strtotime(''.$row['e_date'].' '.$row['e_time'].'');
                    if (($start >= $booking_start && $start <= $booking_end) || ($end >= $booking_start && $end <= $booking_end))
                    {
                        $slot_end = $start;

                        $slot_diff = $start - $slot_start;

                        if ($slot_diff >= $min_time_diff)
                        {
                            $slot_start_date = date("Y-m-d",$slot_start);
                            $slot_start_time = date("G:i:s",$slot_start);
                            $slot_end_date = date("Y-m-d",$slot_end);
                            $slot_end_time = date("G:i:s",$slot_end);

                            $slots["".$count.""] = array(
                                "slot_start_d"=> $slot_start_date,
                                "slot_start_t"=> $slot_start_time,
                                "slot_end_d"=> $slot_end_date,
                                "slot_end_t"=> $slot_end_time
                            );


                            $count = $count + 1;
                        }

                        $slot_start = $end;
                    }
                }

                if ($flag == 0)
                {
                    $slots[$count] = array(
                        "slot_start_d"=> $from_date,
                        "slot_start_t"=> $from_time,
                        "slot_end_d"=> $to_date,
                        "slot_end_t"=> $to_time
                    );
                }
                else if ($slot_start <= $booking_end)
                {
                    $slot_end = $booking_end;
                    $slot_diff = $slot_end - $slot_start;

                    if ($slot_diff >= $min_time_diff)
                    {
                        $slot_start_date = date("Y-m-d",$slot_start);
                        $slot_start_time = date("G:i:s",$slot_start);
                        $slot_end_date = date("Y-m-d",$slot_end);
                        $slot_end_time = date("G:i:s",$slot_end);

                        $slots["".$count.""] = array(
                            "slot_start_d"=> $slot_start_date,
                            "slot_start_t"=> $slot_start_time,
                            "slot_end_d"=> $slot_end_date,
                            "slot_end_t"=> $slot_end_time
                        );

                        $count = $count + 1;
                    }
                }

                echo json_encode($slots);

                break;

            case 6:
                //add_unavailable($from_date,$ground_id)
                if ( $sub_id == '1')
                {
                    $from_time = "00:00:00";
                    $to_date = $from_date;
                    $to_time = "23:59:59";
                }
                else if ( $sub_id == '2')
                {
                    $to_date = $from_date;
                }

                //Set booking id to unavailable
                $booking_id = '0';

                $query = "INSERT INTO bookings(booking_id,ground_id,s_date,s_time,e_date,e_time) VALUES (".$booking_id.",".$ground_id.",'".$from_date."','".$from_time."','".$to_date."','".$to_time."')";

                $result=$write->query($query);

                if($result)
                {
                    echo 1;
                }
                else
                {
                    echo 0;
                }

                break;

            case 7:
                //get_unavailable($from_date,$to_date,$ground_id)

                //Unavailable array
                $unavailable = array();

                $count = 0;

                $query = "SELECT * FROM bookings WHERE booking_id = 0 AND ground_id = ".$ground_id." AND s_date >= '".$from_date."' AND e_date <= '".$to_date."' ORDER BY s_date, s_time";

                $result = $read->fetchAll($query);

                //Get unavailable days
                foreach($result as $row)
                {
                    $unavailable[$count] = array(
                        "start_d"=> $row['s_date'],
                        "start_t"=> $row['s_time'],
                        "end_d"=> $row['e_date'],
                        "end_t"=> $row['e_time']
                    );

                    $count = $count + 1;
                }

                echo json_encode($unavailable);

                break;

            case 8:
                //book_ground_temp($booking_id,$ground_id,$from__date,$from__time,$hours)
                //Prepare end date and time
                if( $sub_id == '1')
                {
                    $end_date_time = strtotime(''.$from_date.' '.$from_time.' + '.$hours.' hours');
                    $e_date = date("Y-m-d",$end_date_time);
                    $e_time = date("G:i:s",$end_date_time);
                }
                else if( $sub_id == '2')
                {
                    $s_time = "00:00:00";
                    $e_date = $from_date;
                    $e_time = "23:59:59";
                }
                else if( $sub_id == '3')
                {
                    $e_date = $from_date;
                }

                $a_date = date("Y-m-d");
                $a_time = date("G:i:s");

                $expiry_time = strtotime('1970-01-01 '.$con_min_confirmation_time.'');
                $add_time = strtotime(''.$a_date.' '.$a_time.'');
                $expiry_value = $add_time + $expiry_time;

                $x_date = date("Y-m-d",$expiry_value);
                $x_time = date("G:i:s",$expiry_value);

                $available = $this->check_available('3',$ground_id,$from_date,$from_time,$to_date,$to_time,$con);
                if( $available != '1')
                {
                    echo '20';
                    exit;
                }
                //Save booking
                $query = "INSERT INTO temp_book(booking_id,ground_id,s_date,s_time,e_date,e_time,a_date,a_time,x_date,x_time) VALUES (".$booking_id.",".$ground_id.",'".$from_date."','".$from_time."','".$e_date."','".$e_time."','".$a_date."','".$a_time."','".$x_date."','".$x_time."')";

                $result = $write->query($query);

                if( $result )
                {
                    echo 1;
                }
                else
                {
                    echo 0;
                }
                break;

        }

        //Close connection
//        mysqli_close($con);

    }

    function connect_booking_db()
    {
        //Create connection
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $write = $resource->getConnection('core_write');


        if (!isset($resource) && !isset($read) && !isset($write))
        {
            exit("Failed to connect to MySQL: " );
        }
        else
        {
            return '';
        }
    }

    function check_available($sub_id,$ground_id,$from_date,$from_time,$to_date,$to_time,$con)
    {

//        $bookings = Mage::getModel('bookingmodule/bookings');
//        $temp_book = Mage::getModel('bookingmodule/temp');
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $write = $resource->getConnection('core_write');
        $booking_start = strtotime(''.$from_date.' '.$from_time.'');

        if ( $sub_id == '1')
        {
            $booking_end = strtotime(''.$from_date.' '.$to_time.'');
        }
        else if ( $sub_id == '2')
        {
            $booking_end = strtotime(''.$from_date.' '.$from_time.' + '.$hours.' hours');
        }
        else if ( $sub_id == '3')
        {
            $booking_end = strtotime(''.$to_date.' '.$to_time.'');
        }

        $bookings = $resource->getTableName('bookingmodule/bookings');
        //Get bookings for the respective ground
        $query = "SELECT * FROM bookings WHERE ground_id = ".$ground_id."";
        $result = $read->fetchAll($query);


        $unavail = 0;

        //Get availability if exist
        foreach($result as $row)
        {
            $start = strtotime(''.$row['s_date'].' '.$row['s_time'].'');
            $end = strtotime(''.$row['e_date'].' '.$row['e_time'].'');

            if ((($booking_start < $start) && ($booking_end <= $start)) || (($booking_start >= $end) && ($booking_end > $end)))
            {
                continue;
            }
            else
            {
                $unavail = 1;
                return '0';
                break;
            }
        }
        //Delete expired bookings
        $this->delete_expired_bookings($con);

        //Get temporary bookings for the respective ground
        if ( $unavail == 0 )
        {
            $query = "SELECT * FROM temp_book WHERE ground_id = ".$ground_id."";
            $result = $read->fetchAll($query);

            //Get availability if exist
            foreach( $result as $row )
            {
                $start = strtotime(''.$row['s_date'].' '.$row['s_time'].'');
                $end = strtotime(''.$row['e_date'].' '.$row['e_time'].'');

                if ((($booking_start < $start) && ($booking_end <= $start)) || (($booking_start >= $end) && ($booking_end > $end)))
                {
                    continue;
                }
                else
                {
                    $unavail = 1;
                    return '0';
                    break;
                }
            }

        }

        //No bookings
        if($unavail == 0)
        {
            return '1';
        }
    }
    function delete_expired_bookings($con)
    {
//        $bookings = Mage::getModel('bookingmodule/bookings');
//        $temp_book = Mage::getModel('bookingmodule/temp');
        $resource = Mage::getSingleton('core/resource');
        $read = $resource->getConnection('core_read');
        $write = $resource->getConnection('core_write');
        $query = "SELECT * FROM temp_book";
        $result = $read->fetchAll($query);
        $c_date = date("Y-m-d");
        $c_time = date("G:i:s");
        $c_date_time = strtotime(''.$c_date.' '.$c_time.'');

        foreach( $result as $row )
        {
            $exp = strtotime(''.$row['x_date'].' '.$row['x_time'].'');

            if ( $exp <= $c_date_time )
            {
                $query = "DELETE FROM temp_book WHERE temporary_id = ".$row['temporary_id']."";
                $write->query($query);
            }
        }
    }
}