<?php
/**
 * Created by PhpStorm.
 * User: projectx
 * Date: 28/5/14
 * Time: 8:55 PM
 */

class Booking_BookingModule_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction ()
    {

//        $data = $this->getRequest()->getPost();
        $error_code = "0";
        $helper = Mage::helper('bookingmodule');

//        $error_code = $helper->validate_url($_SERVER['HTTP_REFERER']);
        if ( $error_code != '0')
        {
            echo $error_code;
            exit;
        }


        //sample data for test
//        $data["fnc_id"] = 1;
//        $data["booking_id"] = 1;
//        $data["ground_id"] = 1;
//        $data["from_date"] ='24-05-2014';
//        $data["from_time"] = '12:00:00';
//        $data["to_date"] = '24-05-2014';
//        $data["to_time"] = '13:00:00';
//        $data["hours"] = 0;
        ///////

        if( isset($data['fnc_id']) && isset($data['booking_id']) && isset($data['ground_id']) && isset($data['from_date']) && isset($data['from_time']) && isset($data['to_date']) && isset($data['to_time']) && isset($data['hours']))
        {
        }
        else
        {
            echo '100'; //Invalid request
            exit;
        }

        $fnc_id = $data["fnc_id"];
        $booking_id = $data["booking_id"];
        $ground_id = $data["ground_id"];
        $from_date = $data["from_date"];
        $from_time = $data["from_time"];
        $to_date = $data["to_date"];
        $to_time = $data["to_time"];
        $hours = $data["hours"];

        $sub_id    = '';
//        $sub_id    = '1';

//Check Function id

        $error_code = $helper->validate_function_id($fnc_id);

        if ( $error_code != '0')
        {
            echo $error_code;
            exit;
        }
        ////////////
//        $helper->booking_db($fnc_id,$sub_id,$booking_id,$ground_id,$from_date,$from_time,$to_date,$to_time,$hours);
        ////////////////
        if( $fnc_id == '3') //Delete booking
        {
            if( $booking_id != "")
            {
                //Check Booking Id
                $error_code = $helper->validate_booking_id($booking_id);
                if ( $error_code != '0')
                {
                    echo $error_code;
                    exit;
                }
                else
                {
                    $fnc_id = '4';
                    $helper->booking_db($fnc_id,$sub_id,$booking_id,$ground_id,$from_date,$from_time,$to_date,$to_time,$hours);
                    exit;
                }
            }
            else
            {
                echo "11";
                exit;
            }
        }
        else
        {
            if( $fnc_id == '2' || $fnc_id == '7')
            {
                if( $booking_id != "")
                {
                    //Check Booking Id
                    $error_code = $helper->validate_booking_id($booking_id);
                    if ( $error_code != '0')
                    {
                        echo $error_code;
                        exit;
                    }
                }
                else
                {
                    echo "11";
                    exit;
                }
            }

            if( $ground_id != "")
            {

                //Check Ground Id
                $error_code = $helper->validate_ground_id($ground_id);

                if ( $error_code != '0')
                {
                    echo $error_code;
                    exit;
                }

            }
            else
            {
                echo "12";
                exit;
            }

            //Check From Date
            if( $from_date != "")
            {

                $error_code = $helper->validate_from_date($from_date);

                if ( $error_code != '0')
                {
                    echo $error_code;
                    exit;
                }

            }
            else
            {
                echo "13";
                exit;
            }

            if ( $fnc_id == '1' || $fnc_id == '2' || $fnc_id == '5' || $fnc_id == '7' )
            {
                if ( $from_time != '' )
                {
                    //Check From Time

                    $error_code = $helper->validate_from_time($from_time);
                    if ( $error_code != '0')
                    {
                        echo $error_code;
                        exit;
                    }
                }
            }
        }

        switch ($fnc_id)
        {
            case 1:
                if ( $from_time != "" )
                {
                    if ( $to_date != "" && $to_time != "" )
                    {
                        $error_code = $helper->validate_to_date($from_date,$to_date);
                        if ( $error_code == '0')
                        {
                            $error_code = $helper->validate_to_time($from_time,$to_time);
                            if ( $error_code == '0')
                            {
                                $fnc_id = "1";
                                $sub_id = "3";
                            }
                            else
                            {
                                echo $error_code;
                                exit;
                            }
                        }
                        else
                        {
                            echo $error_code;
                            exit;
                        }
                    }
                    else if ( $to_time !="" )
                    {
                        $error_code = $helper->validate_to_time($from_time,$to_time);
                        if ( $error_code == '0')
                        {
                            $fnc_id = "1";
                            $sub_id = "1";
                        }
                        else
                        {
                            echo $error_code;
                            exit;
                        }
                    }
                    else if ( $hours != "" )
                    {
                        $fnc_id = "1";
                        $sub_id = "2";
                    }
                    else
                    {
                        echo "10";
                        exit;
                    }
                }
                else //if ( $from_date != "")
                {
                    $fnc_id = "2";
                }
                break;

            case 2:
                if ( $from_time != "" )
                {
                    if ( $to_date != "" && $to_time != "" )
                    {
                        $error_code = $helper->validate_to_date($from_date,$to_date);
                        if ( $error_code == '0')
                        {
                            $error_code = $helper->validate_to_time($from_time,$to_time);
                            if ( $error_code == '0')
                            {
                                $fnc_id = "3";
                                $sub_id = "4";
                            }
                            else
                            {
                                echo $error_code;
                                exit;
                            }
                        }
                        else
                        {
                            echo $error_code;
                            exit;
                        }
                    }
                    else if ( $to_time !="" )
                    {
                        $error_code = $helper->validate_to_time($from_time,$to_time);
                        if ( $error_code == '0')
                        {
                            $fnc_id = "3";
                            $sub_id = "3";
                        }
                        else
                        {
                            echo $error_code;
                            exit;
                        }
                    }
                    else if ( $hours != "" )
                    {
                        $fnc_id = "3";
                        $sub_id = "1";
                    }
                    else
                    {
                        echo "10";
                        exit;
                    }
                }
                else //if ( $from_date != "")
                {
                    $fnc_id = "3";
                    $sub_id = "2";
                }
                break;

            case 4:
                if ( $to_date != "" )
                {
                    $error_code = $helper->validate_to_date($from_date,$to_date);
                    if ( $error_code == '0')
                    {
                        $fnc_id = "5";
                    }
                    else
                    {
                        echo $error_code;
                        exit;
                    }
                }
                else
                {
                    echo "10";
                    exit;
                }
                break;

            case 5:
                if ( $from_time != "")
                {
                    if ( $to_time != "" )
                    {
                        $error_code = $helper->validate_to_time($from_time,$to_time);
                        if ( $error_code == '0')
                        {
                            $fnc_id = "6";
                            $sub_id = "2";
                        }
                        else
                        {
                            echo $error_code;
                            exit;
                        }
                    }
                    else
                    {
                        echo "10";
                        exit;
                    }
                }
                else
                {
                    $fnc_id = "6";
                    $sub_id = "1";
                }
                break;

            case 6:
                if ( $to_date != "" )
                {
                    $error_code = $helper->validate_to_date($from_date,$to_date);
                    if ( $error_code == '0')
                    {
                        $fnc_id = "7";
                    }
                    else
                    {
                        echo $error_code;
                        exit;
                    }
                }
                else
                {
                    echo "10";
                    exit;
                }
                break;

            case 7:
                if ( $from_time != "" )
                {
                    if ( $to_date != "" && $to_time != "" )
                    {
                        $error_code = $helper->validate_to_date($from_date,$to_date);
                        if ( $error_code == '0')
                        {
                            $error_code = $helper->validate_to_time($from_time,$to_time);
                            if ( $error_code == '0')
                            {
                                $fnc_id = "8";
                            }
                            else
                            {
                                echo $error_code;
                                exit;
                            }
                        }
                        else
                        {
                            echo $error_code;
                            exit;
                        }
                    }
                    else if ( $to_time !="" )
                    {
                        $error_code = $helper->validate_to_time($from_time,$to_time);
                        if ( $error_code == '0')
                        {
                            $fnc_id = "8";
                            $sub_id = "3";
                        }
                        else
                        {
                            echo $error_code;
                            exit;
                        }
                    }
                    else if ( $hours != "" )
                    {
                        $fnc_id = "8";
                        $sub_id = "1";
                    }
                    else
                    {
                        echo "10";
                        exit;
                    }
                }
                else //if ( $from_date != "")
                {
                    $fnc_id = "8";
                    $sub_id = "2";
                }
                break;
        }

        if ( $error_code == "0")
        {
            $helper->booking_db($fnc_id,$sub_id,$booking_id,$ground_id,$from_date,$from_time,$to_date,$to_time,$hours);
        }

    }
    public function testModelAction() {
        $bookings = Mage::getModel('bookingmodule/bookings');
        $temp_book = Mage::getModel('bookingmodule/temp');

        echo get_class($bookings);
        echo get_class($temp_book);
//
//        $params = $this->getRequest()->getParams();
//        $blogpost = Mage::getModel('bookingmodule/bookings');
//        echo("Loading the blogpost with an ID of ".$params['id']);
//        $blogpost->load($params['id']);
//        $data = $blogpost->getData();
//        var_dump($data);
    }
    public function showAllBlogPostsAction() {
//        $posts = Mage::getModel('bookingmodule/bookings')->getCollection();
//        foreach($posts as $blogpost){
//            echo '<h3>'.$blogpost->getTransaction_id().'</h3>';
//            echo nl2br($blogpost->getBooking_id());
//        }
//
    }
}