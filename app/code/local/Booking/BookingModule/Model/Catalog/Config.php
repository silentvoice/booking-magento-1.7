<?php
/**
 * Created by PhpStorm.
 * User: projectx
 * Date: 2/6/14
 * Time: 2:46 AM
 */
class Booking_BookingModule_Model_Catalog_Config extends Mage_Catalog_Model_Config
{
    public function getAttributeUsedForSortByArray()
    {
        $options = array(
            'popularity'  => Mage::helper('catalog')->__('Popularty'),
            'topsellings'  => Mage::helper('catalog')->__('Top Selling')
        );
        foreach ($this->getAttributesUsedForSortBy() as $attribute) {
            /* @var $attribute Mage_Eav_Model_Entity_Attribute_Abstract */
            $options[$attribute->getAttributeCode()] = $attribute->getStoreLabel();
        }
        return $options;
    }
}