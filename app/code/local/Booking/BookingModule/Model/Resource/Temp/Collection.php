<?php
class Booking_BookingModule_Model_Resource_Temp_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function _construct()
    {
        $this->_init('bookingmodule/temp');
    }
}