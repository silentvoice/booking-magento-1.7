<?php
/**
 * Created by PhpStorm.
 * User: projectx
 * Date: 2/6/14
 * Time: 2:49 AM
 */
class Booking_BookingModule_Model_Resource_Product_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    public function sortByReview($dir){
        $table = $this->getTable('review/review');
        $entity_code_id = Mage::getModel('review/review')->getEntityIdByCode(Mage_Rating_Model_Rating::ENTITY_PRODUCT_CODE);
        $cond = $this->getConnection()->quoteInto('t2.entity_pk_value = e.entity_id and ','').$this->getConnection()->quoteInto('t2.entity_id = ? ',$entity_code_id);
        $this->getSelect()->joinLeft(array('t2'=>$table), $cond,array('review' => new Zend_Db_Expr('count(review_id)')))
            ->group('e.entity_id')->order("review $dir");
    }
}