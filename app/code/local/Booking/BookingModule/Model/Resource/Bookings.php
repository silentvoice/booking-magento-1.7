<?php
class Booking_BookingModule_Model_Resource_Bookings extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('bookingmodule/bookings', 'transaction_id');
    }
}