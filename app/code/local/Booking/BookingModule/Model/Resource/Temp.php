<?php
class Booking_BookingModule_Model_Resource_Temp extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('booking_bookingmodule/temp', 'temporary_id');
    }
}