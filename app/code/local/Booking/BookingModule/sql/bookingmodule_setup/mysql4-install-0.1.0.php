<?php
$installer = $this;
$installer->startSetup();
$installer->run("
     CREATE TABLE `{$installer->getTable('bookingmodule/bookings')}` (
  `transaction_id` int(10) NOT NULL AUTO_INCREMENT,
  `booking_id` int(5) NOT NULL,
  `ground_id` int(5) NOT NULL,
  `s_date` date NOT NULL,
  `s_time` time DEFAULT NULL,
  `e_date` date DEFAULT NULL,
  `e_time` time DEFAULT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;
    INSERT INTO `{$installer->getTable('bookingmodule/bookings')}` VALUES (5, 10, 1, '2014-05-20', '00:00:00', '2014-05-20', '23:59:59'),
(6, 10, 1, '2014-05-23', '12:00:00', '2014-05-23', '14:00:00'),
(7, 10, 1, '2014-05-22', '15:00:00', '2014-05-22', '20:00:00');
CREATE TABLE  `{$installer->getTable('bookingmodule/temp')}` (
  `temporary_id` int(10) NOT NULL AUTO_INCREMENT,
  `booking_id` int(5) NOT NULL,
  `ground_id` int(5) NOT NULL,
  `s_date` date NOT NULL,
  `s_time` time DEFAULT NULL,
  `e_date` date DEFAULT NULL,
  `e_time` time DEFAULT NULL,
  `a_date` date NOT NULL,
  `a_time` time NOT NULL,
  `x_date` date NOT NULL,
  `x_time` time NOT NULL,
  PRIMARY KEY (`temporary_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

");
$installer->endSetup();